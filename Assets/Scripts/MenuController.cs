﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public sealed class MenuController : MonoBehaviour {
    [SerializeField] private Text title = null;
    [SerializeField] private Text score = null;
    [SerializeField] private Button play = null;
    [SerializeField] private Button quit = null;
    
    private void Start() {
        UpdateScore();

        play.onClick.AddListener(() => SceneManager.LoadScene(1));
        quit.onClick.AddListener(Application.Quit);
    }

    private void UpdateScore() {
        if (SessionController.LastSessionPlayerScore.HasValue) {
            score.gameObject.SetActive(true);
            var lastScore = SessionController.LastSessionPlayerScore.Value;
            var lastScoreText = $"Last score: {SessionController.LastSessionPlayerScore.Value.ToString()}";
            
            // NOTE: Duke's phrases
            if (lastScore < 10)
                score.text = lastScoreText + "\nDamn, you're ugly";
            else if (lastScore > 50)
                score.text = lastScoreText + "\nBitching'";
            else
                score.text = lastScoreText + "\nYeah, piece of cake!";
        }
        else 
            score.gameObject.SetActive(false);
    }

    private void Update() => title.color = Random.ColorHSV();
}