﻿using System;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class PlayerShip : Ship, IDestructable {
    [Header("Laser")] 
    [SerializeField] public float laserMaxCharge = 5f;
    [SerializeField] private float laserRechargePerSecond = 0.4f;
    [SerializeField] private float laserShootCooldown = 0.5f;
    [SerializeField] private string[] laserRayCastLayers = null;
    
    public float LaserCharge { get; private set; }
    public event Action OnDestruct;
    
    private LayerMask laserDamageMask;
    private LineRenderer laserRenderer;
    private float laserLastShootTime = float.MinValue;
    private float laserLength;
    private readonly RaycastHit2D[] laserHits = new RaycastHit2D[3];

    protected override void Awake() {
        base.Awake();
        laserRenderer = GetComponent<LineRenderer>();
        laserDamageMask = LayerMask.GetMask(laserRayCastLayers);
        LaserCharge = laserMaxCharge;
        laserLength = laserRenderer.GetPosition(1).magnitude;
    }

    private void FixedUpdate() {
        // TODO: Check correct of Input in FixedUpdate
        Move(Input.GetAxis("Vertical"), Input.GetAxis("Horizontal")); 
        
        LaserCharge += Time.fixedDeltaTime * laserRechargePerSecond;
        LaserCharge = Mathf.Min(LaserCharge, laserMaxCharge);
        
        if (laserRenderer.enabled) {
            if (Time.time > laserLastShootTime + laserShootCooldown) 
                laserRenderer.enabled = false;
            else 
                SustainLaserDamage();
        }
    }
    
    private void Update() {
        if (Input.GetKey(KeyCode.Escape)) OnDestruct?.Invoke();
        if (Input.GetButtonDown("Fire1")) ShootProjectile();
        if (Input.GetButtonDown("Fire2")) ActivateLaser();
    }

    private void ActivateLaser() {
        if (laserRenderer.enabled) return;
        if (LaserCharge < 1f) return;
        if (Time.time <= laserLastShootTime + laserShootCooldown) return;

        LaserCharge--;
        laserLastShootTime = Time.time;
        laserRenderer.enabled = true;
    }

    private void SustainLaserDamage() {
        var shipTransform = transform;
        
        Physics2D.RaycastNonAlloc(
            shipTransform.position,
            shipTransform.up,
            laserHits,
            laserLength,
            laserDamageMask
        );

        for (var h = 0; h < laserHits.Length; ++h) {
            if (!ReferenceEquals(laserHits[h].transform, null)) {
                Destroy(laserHits[h].transform.gameObject);
            }
        }
    }

    private void OnDestroy() => OnDestruct?.Invoke();

    private void OnDrawGizmos() =>         Gizmos.DrawLine(transform.position, transform.position + transform.up * 10f);
}