﻿using UnityEngine;

[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public abstract class Ship : MonoBehaviour {
    [Header("Movement")] 
    [SerializeField] private float forwardSpeed = 5f;
    [SerializeField] private float angularSpeed = 5f;

    [Header("Projectiles")] 
    [SerializeField] private Projectile projectilePrefab = null;
    [SerializeField] private float projectileSpawnOffset = 1.5f;
    [SerializeField] private float projectileShootCooldown = 0.5f;

    private Rigidbody2D shipRigidBody;
    private float lastProjectileShootTime = float.MinValue;
    
    protected virtual void Awake() => shipRigidBody = GetComponent<Rigidbody2D>();

    protected void Move(float vertical, float horizontal) {
        if (vertical >= float.Epsilon || vertical <= float.Epsilon) {
            shipRigidBody.AddForce(forwardSpeed * vertical * transform.up, ForceMode2D.Force);
        }

        if (horizontal >= float.Epsilon || horizontal <= float.Epsilon) {
            shipRigidBody.AddTorque(angularSpeed * -horizontal, ForceMode2D.Force);
        }
    }
    
    protected void ShootProjectile() {
        if (Time.time < lastProjectileShootTime + projectileShootCooldown) return;

        lastProjectileShootTime = Time.time;
        SpawnProjectile();
    }

    private void SpawnProjectile() {
        var shipTransform = transform;
        var shipForward = shipTransform.up;
        var shipPosition = shipTransform.localPosition + shipForward * projectileSpawnOffset;
        Instantiate(projectilePrefab, shipPosition, shipTransform.localRotation);
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if (!other.transform.CompareTag("Wall")) {
            Destroy(gameObject);
        }
    }
}