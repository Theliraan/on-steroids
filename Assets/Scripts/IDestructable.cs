﻿using System;

public interface IDestructable {
    event Action OnDestruct;
}
