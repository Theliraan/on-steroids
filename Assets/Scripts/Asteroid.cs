﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public class Asteroid : MonoBehaviour, IDestructable {
    [SerializeField] private float maxStartAxisImpulse = 50f;
    [SerializeField] private float lifetimeSeconds = 30f;

    public event Action OnDestruct;

    private bool IsLifetimeOver => Time.time > lifetimeOver;
    private float lifetimeOver; 
    private Rigidbody2D asteroidRigidBody;

    private void Start() {
        asteroidRigidBody = GetComponent<Rigidbody2D>();
        asteroidRigidBody.AddForce(new Vector2(
            Random.Range(-maxStartAxisImpulse, maxStartAxisImpulse),
            Random.Range(-maxStartAxisImpulse, maxStartAxisImpulse)
        ));
        asteroidRigidBody.AddTorque(Random.Range(-1f, 1f));
        lifetimeOver = Time.time + lifetimeSeconds;
    }

    private void Update() {
        if (IsLifetimeOver) Destroy(gameObject);
    }

    private void OnDestroy() {
        if (!IsLifetimeOver) {
            OnDestruct?.Invoke();
        }
    }
}