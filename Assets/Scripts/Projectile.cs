﻿using UnityEngine;

[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public class Projectile : MonoBehaviour {
    [SerializeField] private float lifetimeSeconds = 3f;
    [SerializeField] private float speed = 10f;

    private float deSpawnTime;

    private void Start() {
        deSpawnTime = Time.time + lifetimeSeconds;
        GetComponent<Rigidbody2D>().velocity = transform.up * speed;
    }

    private void Update() {
        if (Time.time >= deSpawnTime) {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if (!other.transform.CompareTag("Wall")) {
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }
}