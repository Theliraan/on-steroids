﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SessionController : MonoBehaviour {
    public static uint? LastSessionPlayerScore;
    
    [Header("UI")] 
    [SerializeField] private Text scoreText = null;
    [SerializeField] private Scrollbar laserCharge = null;

    [Header("Prefabs")] 
    [SerializeField] private PlayerShip playerShipPrefab = null;
    [SerializeField] private EnemyShip enemyShipPrefab = null;
    [SerializeField] private Asteroid smallAsteroidPrefab = null;
    [SerializeField] private Asteroid asteroidPrefab = null;
    
    [Header("Spawn")] 
    [SerializeField] private uint smallAsteroidCount = 2;
    [SerializeField] private float minSpawnRange = 3f;
    [SerializeField] private float maxSpawnRange = 10f;
    [SerializeField] private uint asteroidStartCount = 3;
    [SerializeField] private float asteroidSpawnCooldownSeconds = 3f;
    [SerializeField] private uint enemyStartCount = 1;
    [SerializeField] private float enemySpawnCooldownSeconds = 5f;

    private bool isQuitting;
    private bool isGameOver;
    private PlayerShip playerShip;
    private uint playerScore;

    private void Start() {
        scoreText.text = playerScore.ToString();
        SpawnPlayerShip();
        StartCoroutine(SpawnEnemyShips());
        StartCoroutine(SpawnAsteroids());
    }

    private void Update() => laserCharge.size = playerShip.LaserCharge / playerShip.laserMaxCharge;

    private void OnApplicationQuit() => isQuitting = false;

    private IEnumerator SpawnEnemyShips() {
        var spawnWait = new WaitForSeconds(enemySpawnCooldownSeconds);
        
        for (var e = 0; e < enemyStartCount; e++) {
            SpawnEnemyShip(playerShip.transform.position);
        }
        
        while(true) {
            yield return spawnWait;
            SpawnEnemyShip(playerShip.transform.position);
        }
    }

    private IEnumerator SpawnAsteroids() {
        var spawnWait = new WaitForSeconds(asteroidSpawnCooldownSeconds);
        
        for (var a = 0; a < asteroidStartCount; a++) {
            SpawnAsteroid(playerShip.transform.position);
        }
        
        while(true) {
            yield return spawnWait;
            SpawnAsteroid(playerShip.transform.position);
        }
    }

    private void SpawnPlayerShip() {
        playerShip = Instantiate(playerShipPrefab, Vector3.zero, Quaternion.Euler(Vector3.zero));
        playerShip.OnDestruct += OnGameOver;
    }

    private void SpawnEnemyShip(Vector3 centerPosition) {
        var enemyShip = SpawnOnRadius(centerPosition, enemyShipPrefab);
        enemyShip.PlayerShip = playerShip;
    }

    private void SpawnAsteroid(Vector3 centerPosition) {
        var asteroid = SpawnOnRadius(centerPosition, asteroidPrefab);
        asteroid.OnDestruct += () => SpawnSmallAsteroids(asteroid.transform.position);
    }

    private T SpawnOnRadius<T>(Vector3 centerPosition, T prefab) where T : MonoBehaviour, IDestructable {
        var position = centerPosition + GetRandomPointOnCircle(Random.Range(minSpawnRange, maxSpawnRange));
        var instance = Instantiate(prefab, position, Quaternion.identity);
        instance.OnDestruct += AddScore;
        return instance;
    }

    private void SpawnSmallAsteroids(Vector3 parentPosition) {
        if (isGameOver || isQuitting) return;
        
        for (var a = 0; a < smallAsteroidCount; ++a) {
            Instantiate(smallAsteroidPrefab, parentPosition, Quaternion.identity).OnDestruct += AddScore;
        }
    }

    private void AddScore() {
        if (isGameOver || isQuitting) return;
        
        playerScore++;
        scoreText.text = playerScore.ToString();
    }

    private void OnGameOver() {
        if (isQuitting) return;

        isGameOver = true;
        LastSessionPlayerScore = playerScore;
        StopAllCoroutines();
        SceneManager.LoadScene(0);
    }

    private static Vector3 GetRandomPointOnCircle(float radius) {
        var vector2 = Random.insideUnitCircle.normalized * radius;
        return new Vector3(vector2.x, vector2.y);
    }
}