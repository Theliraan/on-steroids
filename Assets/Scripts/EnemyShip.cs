﻿using System;
using UnityEngine;

public class EnemyShip : Ship, IDestructable {
    [NonSerialized] public PlayerShip PlayerShip;

    public event Action OnDestruct = null;

    private void Update() => ShootProjectile();

    private void FixedUpdate() {
        var myTransform = transform;
        Move(
            1f,
            -Vector3.SignedAngle(
                myTransform.up,
                PlayerShip.transform.position - myTransform.position,
                myTransform.forward
            ) / 180f
        );
    }

    private void OnDestroy() => OnDestruct?.Invoke();
}